# templates

GitLab CI templates

These files are intended to be included in project .gitlab-ci.yml files.

Projects should make use of these templates. Add the following to `.gitlab-ci.yml` files:

```yaml
include: 
  - project: planetrover/templates
    file: templates.yml
```

## docker.yml

Extend these jobs to build or push Docker images to the Gitlab container registry.

- .docker-build
- .docker-push-latest
- .docker-push-latest-branch-only

## helm.yml

Extend these jobs to package or push Helm charts to the GitLab package registry.

- .helm-package
- .helm-package-push

## linters.yml

Extend these jobs in order to lint code in selected files from your project.

- .dockerfile-lint
- .go-lint
- .helm-lint

## package.yml

Extend these jobs to push to or pull from the GitLab package registry.

- .package-push
- .package-pull
